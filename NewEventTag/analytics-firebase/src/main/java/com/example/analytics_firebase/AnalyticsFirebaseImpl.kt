package com.example.analytics_firebase

import android.content.Context
import android.os.Bundle
import com.example.analytics_api.Analytics
import com.google.firebase.analytics.FirebaseAnalytics

class AnalyticsFirebaseImpl(context: Context) : Analytics {
    private val firebaseAnalytics = FirebaseAnalytics.getInstance(context)

    override fun tagEvent(eventAttributes: Map<String, String>) {
        val bundle = Bundle().apply {
            for ((key, value) in eventAttributes) {
                putString(key, value)
            }
        }
        firebaseAnalytics.logEvent("custom_event", bundle)
        firebaseAnalytics.logEvent("Button_Clicked",null)
    }

    override fun tagScreen(screenName: String) {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, Bundle().apply {
            putString(FirebaseAnalytics.Param.SCREEN_NAME, screenName)
        })
    }

    override fun tagProfileAttribute(profileAttributes: Map<String, String>) {
        for ((key, value) in profileAttributes) {
            firebaseAnalytics.setUserProperty(key, value)
        }
    }

}