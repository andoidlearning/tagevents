package com.example.newtaggingapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.example.analytics_api.Analytics
import com.example.analytics_api.AnalyticsImpl
import com.example.analytics_firebase.AnalyticsFirebaseImpl

class MainActivity : AppCompatActivity() {

    private lateinit var analytics: Analytics

    lateinit var editTextName: EditText
    lateinit var editTextEmail: EditText
    lateinit var buttonSave: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //analytics = AnalyticsImpl()
        analytics = AnalyticsFirebaseImpl(this)

        editTextName = findViewById(R.id.editTextName)
        editTextEmail = findViewById(R.id.editTextEmail)
        buttonSave = findViewById(R.id.buttonSave)


        /*analytics.tagEvent(mapOf("event" to "app_open", "timestamp" to System.currentTimeMillis().toString()))
        analytics.tagScreen("MainActivity")
        analytics.tagProfileAttribute(mapOf("user_id" to "12345", "user_name" to "Vishvajeet Bagal"))*/

        buttonSave.setOnClickListener {
            val userName = editTextName.text.toString()
            val userEmail = editTextEmail.text.toString()

            if (userName.isNotEmpty() && userEmail.isNotEmpty()) {
                // Report user details as profile attributes to Firebase
                analytics.tagProfileAttribute(
                    mapOf(
                        "user_name" to userName,
                        "user_email" to userEmail
                    )
                )

                // Tag the submit event
                analytics.tagEvent(
                    mapOf(
                        "event" to "submit",
                        "user_name" to userName,
                        "user_email" to userEmail
                    )
                )

                // Tag the screen view event
                analytics.tagScreen("UserDetailsScreen")
            }

        }
    }
}