package com.example.analytics_api

import android.util.Log

class AnalyticsImpl : Analytics {

    override fun tagEvent(eventAttributes: Map<String, String>) {
        Log.d("Analytics", "Event tagged with attributes: $eventAttributes")
    }

    override fun tagScreen(screenName: String) {
        Log.d("Analytics", "Screen tagged: $screenName")
    }

    override fun tagProfileAttribute(profileAttributes: Map<String, String>) {
        Log.d("Analytics", "Profile attributes tagged: $profileAttributes")
    }

}