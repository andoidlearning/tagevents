package com.example.analytics_api

interface Analytics {

    fun tagEvent(eventAttributes: Map<String, String>)
    fun tagScreen(screenName: String)
    fun tagProfileAttribute(profileAttributes: Map<String, String>)

}